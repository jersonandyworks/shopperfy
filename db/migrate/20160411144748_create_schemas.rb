class CreateSchemas < ActiveRecord::Migration
  def change
    create_table :schemas do |t|
      t.string "domain", :limit => 120
      t.integer "theme_id"
      t.timestamps null: false
    end
  end
end
