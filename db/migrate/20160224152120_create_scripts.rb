class CreateScripts < ActiveRecord::Migration
  def change
    create_table :scripts do |t|
      t.string 'shopify_domain', :limit => 250
  	  t.string 'filename'
  	  t.integer 'script_id'
  	  t.string 'style', :limit => 50
      t.timestamps null: false
    end
  end
end
