ShopifyApp.configure do |config|
  config.api_key = "4bd5b432032fb6aaab42957cedca9ca4"
  config.secret = "4a848e0f4aef7c7a64e976b54b41cdff"
  config.scope = "read_orders, read_products, write_script_tags, read_script_tags,read_themes, write_themes"
  config.embedded_app = true
  config.webhooks = [
    {topic: 'app/uninstalled', address: 'https://shopperfy.herokuapp.com', format: 'json'},
  ]
end
