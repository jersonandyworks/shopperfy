module ApplicationHelper
	def script(id,cycle)
		scriptExists = Script::where(:script_id => id).first
		render(:partial => 'application/scripts', :locals => {:scriptExists => scriptExists,:cycle_values => cycle,:id => id})

	end

	def script_type(id)
		script = Script::where(:script_id => id).first
		render(:partial => 'application/script_type', :locals => {:script => script,:id => id})
	end

	def checkSelectedTheme(id)
		domain = session[:domain]
		if domain 
			selectedTheme = Schema.where(:domain => domain,:theme_id => id).first
			return (selectedTheme)? true : false
		end
	end
end
