class SchemaController < ShopifyApp::AuthenticatedController
	def index
	end
  def activate
  		script = Script.where(:shopify_domain => session[:shopify_domain]).first
	    urlID = script.filename
	  	id = params[:id]
	  	style = params[:style]
		require 'net/http'
	    url = Net::HTTP.get_response(URI.parse("https://www.shopperapproved.com/feeds/schema.php/?siteid=14043&token=9nNBCrWKZFsztbQ")).body
	    @url = url.squish
	    urlSquished = url.squish
	    puts "READING:....."..urlSquished
	    filePath = "public/#{urlID}_schema"+".js"
	  	file = File.exists?(filePath)
	  	if file
	  		File.delete(filePath)
	    	generateScript(urlID,urlSquished,filePath,style)
	    	schema = Schema.where(:domain => "fivedollarshop.myshopify.com").first
	    	if schema
	    		schema.update_attributes(:theme_id => id)
	    		puts 'Script Exists and updated!'
	    	else
	    		Schema.create(:domain => "fivedollarshop.myshopify.com", :theme_id => id)
	    		puts 'Script Exists and created!'
	    	end
	    	
	    else
	    	generateScript(urlID,urlSquished,filePath,style)
	    	puts 'Script Does not Exists!'
	  	end
	  	redirect_to(:action => 'index')
  end

  private
  	def generateScript(urlID,urlSquished,file,style)
  		content = "!function(){var e=function(e,t){var a=document.createElement(\"script\");a.type=\"text/javascript\",a.readyState?a.onreadystatechange=function(){(\"loaded\"==a.readyState||\"complete\"==a.readyState)&&(a.onreadystatechange=null,t())}:a.onload=function(){t()},a.src=e,document.getElementsByTagName(\"head\")[0].appendChild(a)},t=function(e){e(\"footer\").append('<div class=\"url #{style}\" style=\"text-align:center\"></div>'),e(\".url\").html('#{urlSquished}'),e(\".url a[target=shopperapproved]\").css({\"text-decoration\":\"none\",\"font-size\":\"0.9em\",\"font-weight\":\"bold\"})};\"undefined\"==typeof jQuery||parseFloat(jQuery.fn.jquery)<1.7?e(\"//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js\",function(){jQuery191=jQuery.noConflict(!0),t(jQuery191)}):t(jQuery)}();"
  		File.open(file,"w+") do |f|
  			f.write(content)
  		end
  		script_tag = ShopifyAPI::ScriptTag.create(:event => "onload", :src => "https://shopperfy.herokuapp.com/#{urlID}_schema.js", :body => "#{script}")    
	    Script.new(:shopify_domain => session[:shopify_domain],:style => "schema", :filename => script.filename + "_schema", :script_id => script_tag.id).save

	     # Add Custom CSS
	    style_tag = ShopifyAPI::ScriptTag.create(:event => "onload", :src => "https://shopperfy.herokuapp.com/css/style_custom.js")
	    Script.new(:shopify_domain => session[:shopify_domain],:style => "css", :filename => "style_custom.css", :script_id => style_tag.id).save
  	end

end
