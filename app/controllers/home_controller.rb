class HomeController < ShopifyApp::AuthenticatedController
  def index
    # @products = ShopifyAPI::Product.find(:all, :params => {:product_type => "Underarmour"})
    if session[:shopify_domain] == nil
      domain = params[:shop]
      session[:shopify_domain] = domain
    end
    @domainCheck = Script.where(:shopify_domain => session[:shopify_domain],:style => 'checkout').first
    @scripts = ShopifyAPI::ScriptTag.all
    # @scripts = Script.where(:shopify_domain => session[:shopify_domain])
   

  end

  def create
   
    script = subject_params[:name]
    domainSession = subject_params[:domain]
    filePath = "public/#{script}.js"
    domainCheck = Script.where(:shopify_domain => session[:shopify_domain],:style => 'checkout').first
    # Check if the domain has records scripts
    if domainCheck
      flash[:notice] = "You can now receive reviews on your Thank you page!"
    else
        content = 'if(window.location.href.indexOf("thank_you") > -1) {var sa_values = { "site":'"#{script}"', "forcecomments":1 };  function saLoadScript(src) { var js = window.document.createElement("script"); js.src = src; js.type = "text/javascript"; document.getElementsByTagName("head")[0].appendChild(js); } var d = new Date(); if (d.getTime() - 172800000 > 1454671334000) saLoadScript("https://www.shopperapproved.com/thankyou/rate/'"#{script}.js"'"); else saLoadScript("https://direct.shopperapproved.com/thankyou/rate/'"#{script}"'.js?d=" + d.getTime());}'    
        File.open(filePath,'w+') do |f|
          f.write(content)
        end
        
        script_tag = ShopifyAPI::ScriptTag.create(:event => "onload", :src => "https://shopperfy.herokuapp.com/#{script}.js", :body => "#{script}")
        Script.new(:shopify_domain => domainSession, :style => "checkout", :filename => script, :script_id => script_tag.id).save
        flash[:notice] = "Shopper Approved Script Generated to Checkout page!"

    end
    redirect_to(:action => 'index')
  end


  def delete
    script = Script.exists?(:shopify_domain => session[:shopify_domain])
    if script
      # Script.where(:shopify_domain => session[:shopify_domain]).destroy_all
      Script.where(:script_id => params[:id]).destroy_all
      # session.delete(:shopify_domain)
    end
    src = ShopifyAPI::ScriptTag.delete(params[:id])

    flash[:notice] = "Shopper Approved Script Removed from Checkout page!"

    redirect_to(:action => 'index')
  end
  # removes the current shopify domain on scripts by session
  def reactivate
    domain = session[:shopify_domain]
    Script.where(:shopify_domain => domain).destroy_all
    redirect_to(:action => 'index')
  end

  private
      def subject_params
        params.require(:home).permit(:name,:domain)
      end
end
